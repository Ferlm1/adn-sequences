from flask import Flask
from resources.routes import initialize_routes
from flask_restful import Api

app = Flask(__name__)
api = Api(app)

initialize_routes(api)



app.run()