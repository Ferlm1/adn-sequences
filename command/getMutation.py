from database.bd import session
import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key

def getMutation(dynamodb=None):
    if not dynamodb:
        dynamodb = session.resource('dynamodb', region_name='us-east-2')
    table = dynamodb.Table('ADN')

    try:
        hasMutationResponse = table.query(KeyConditionExpression=Key('hasMutation').eq(1))
        NoHasMutationResponse = table.query(KeyConditionExpression=Key('hasMutation').eq(0))
        count_mutations = hasMutationResponse['Count']
        count_no_mutation = NoHasMutationResponse['Count']
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        return {
            "count_mutations": count_mutations,
            "count_no_mutation": count_no_mutation,
            "ratio": count_mutations/count_no_mutation
        }