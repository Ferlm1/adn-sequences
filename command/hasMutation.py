from .pila import Pila
from database.bd import session
import boto3
"""
This file analyses dna sequence in order to know if has mutation

"""

def routeMutation(dna, dynamodb=None):
    if not dynamodb:
        dynamodb = session.resource('dynamodb', region_name='us-east-2')
    table = dynamodb.Table('ADN')

    mutation = ""
    item = 0
    if hasMutation(dna):
        item = 1

    for d in dna["dna"]:
        mutation = mutation + d

    print(item)
    table.put_item(
       Item={
            'hasMutation': item,
            'dna': mutation,
        }
    )
    if item == 1:
        return {'id': True},200
    return {'id': False},403

# This functions make all mutation analysis: Vertical, Horizontal, oblicual, oblicual Inverse 

def hasMutation(dna):
    # dna copies due to some logic functions which affect the array 
    dna_copy = dna["dna"].copy()
    dna_copy2 = dna["dna"].copy()
    dna_copy3 = dna["dna"].copy()

    if horizontalMutation(dna["dna"]):
        return True
    if verticalMutation(dna["dna"], 0):
        return True
    if oblicualMutation(dna_copy):
        return True
    if oblicualMutation2(dna_copy2):
        return True
    if InverseOblicualMutation(dna_copy3, 1):
        return True
    return InverseOblicualMutation2(dna_copy3, len(dna["dna"])-2)

def horizontalMutation(dna):
    pila = Pila()
    for d in dna:
        for n in d:
            if (pila.tamano() < 4):
                if not pila.estaVacia():
                    if n != pila.inspeccionar():
                        pila.vaciar()
                        pila.incluir(n)
                    else:
                        pila.incluir(n)
                else: 
                    pila.incluir(n)
            else:
                return True
        if pila.tamano() > 3:
            return True
        pila.vaciar()

    return False

def verticalMutation(dna, j: int):
    pila = Pila()
    if j >= len(dna):
        return False
    for d in dna:
        if pila.tamano() > 3:
            return True
        if not pila.estaVacia():
            if d[j] != pila.inspeccionar():
                pila.vaciar()
                pila.incluir(d[j])
            else:
                pila.incluir(d[j])
        else: 
            pila.incluir(d[j])
        
    return verticalMutation(dna, j+1)


def oblicualMutation(dna):
    pila = Pila()
    j = 0
    if not dna:
        return False
    for d in dna:
        if pila.tamano() > 3:
            return True
        if j < len(dna):
            if not pila.estaVacia():
                if d[j] != pila.inspeccionar():
                    pila.vaciar()
                    pila.incluir(d[j])
                else:
                    pila.incluir(d[j])
            else:
                pila.incluir(d[j])
        j = j + 1
    dna.pop(0)
    if pila.tamano() > 3:
        return True
    return oblicualMutation(dna)

def oblicualMutation2(dna):
    pila = Pila()
    if not dna:
        return False
    j = len(dna[0]) - 1
    for d in dna:
        if pila.tamano() > 3:
            return True  
        if j >= 0:
            if not pila.estaVacia():
                if d[j] != pila.inspeccionar():
                    pila.vaciar()
                    pila.incluir(d[j])
                else:
                    pila.incluir(d[j])
            else:
                pila.incluir(d[j])
        j = j - 1
    dna.pop(0)
    if pila.tamano() > 3:
        return True
    return oblicualMutation2(dna)

def InverseOblicualMutation(dna, j: int):
    pila = Pila()
    if j == len(dna):
        return False
    i = j
    for d in dna:
        if pila.tamano() > 3:
            return True
        if i < len(dna):
            if not pila.estaVacia():
                if d[i] != pila.inspeccionar():
                    pila.vaciar()
                    pila.incluir(d[i])
                else:
                    pila.incluir(d[i])
            else:
                pila.incluir(d[i])
        i = i + 1
    if pila.tamano() > 3:
        return True
    return InverseOblicualMutation(dna, j+1)

def InverseOblicualMutation2(dna, j: int):
    pila = Pila()
    if j < 0:
        return False
    i = j
    for d in dna:
        if pila.tamano() > 3:
            return True
        if i >= 0:
            if not pila.estaVacia():
                if d[i] != pila.inspeccionar():
                    pila.vaciar()
                    pila.incluir(d[i])
                else:
                    pila.incluir(d[i])
            else:
                pila.incluir(d[i])
        i = i - 1
    if pila.tamano() > 3:
        return True
    return InverseOblicualMutation2(dna, j-1)