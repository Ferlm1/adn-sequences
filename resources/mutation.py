from flask import Response, request
from flask_restful import Resource
from command.hasMutation import routeMutation
from command.getMutation import getMutation

class Mutation(Resource):
    def get(self):
        return getMutation()
    
    def post(self):
        dna = request.get_json()
        return routeMutation(dna)
