from .mutation import Mutation

def initialize_routes(api):
 api.add_resource(Mutation, '/mutation')